//soal 1
let luas= (a, b) => a*b;
let keliling =(a, b) => 2*(a*b);
console.log(luas(5,4));
console.log(keliling(2,3));
//soal 2
let newFunction2 = (first , last) => console.log(first + " " + last);
newFunction2("William", "Imoh")

//soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
const {firstName, lastName, address, hobby} = newObject;
console.log(firstName, lastName, address, hobby)

//soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west , ...east];
console.log(combined)

//soal 5
const planet = "earth" 
const view = "glass" 
const str = `Lorem ${view} dolor sit amet consectetur adipiscing elit, ${planet}`;
console.log(str);

