//soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
for(var i = 0; i < 5 ; i++){
    console.log(daftarHewan[i]);
}

//soal 2
function introduce(data){
    console.log("Nama saya " + data.name + ", umur saya " + data.age +" tahun, alamat saya di " + data.address
    + ", dan saya punya hobby yaitu " + data.hobby + "!");
}
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)

//soal 3
function hitung_huruf_vokal(kata){
    var panjang = kata.length;
    var total = 0;
    for(var i = 0 ; i < panjang ; i++){
        if(kata[i] == "a" || kata[i] == "i" || kata[i] == "u"|| kata[i] == "e"|| kata[i] == "o"){
            total = total + 1;
        }
    }
    console.log(total);
}
var kata = "ciken";
hitung_huruf_vokal(kata);

//soal 4
function hitung(angka){
return angka * 5 + 7
}
var jawaban = hitung(5);
console.log(jawaban);