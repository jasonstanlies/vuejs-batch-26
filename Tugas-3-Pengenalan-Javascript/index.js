// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var ketiga = pertama.substr(0,4) + " " + pertama.substr(12,6) + " ";
var keempat = kedua.substr(0,7) + " " + kedua.substr(8,10);
console.log(ketiga + keempat);

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
var satu = parseInt(kataPertama);
var dua = parseInt(kataKedua);
var tiga = parseInt(kataKetiga);
var empat = parseInt(kataKeempat);
var ans = (satu+dua)*(empat-tiga);
console.log(ans);

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 
var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substr(4,10);
var kataKetiga = kalimat.substr(15,3);
var kataKeempat = kalimat.substr(19,5);
var kataKelima = kalimat.substr(25,6);

//sekian jawaban dari saya, terima kasih
//Nama : Jason Stanlie